#!/usr/bin/env python


################################################################################
# This software uses BeautifulHue
# 
# 
# BeautifulHue License
# 
# Copyright (c) 2013 Allan Bunch
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE
# 
################################################################################


'''
    File name: actuator.py
    Author(s): Bernardo Abreu and Matheus Nunes
    Date created: Jan 2017
    Date last modified: Jan 17, 2017
    Python version: 2.7
'''

#Networking
import connection
# argparse to parse the argunments for the logging
import argparse
#Logging
import logging
import vc_logging
#Time
import timeit
#Lights
from lights import Light

class Actuator():
    """ Actuator node class."""


    def __init__(self, args):
        vc_logging.init_logger(level = args.log_level, verbose = args.verbose)
        self.log = logging.getLogger("vc_logger")

        self.color = {
                        'red'       : 0,
                        'yellow'    : 14000,
                        'green'     : 25500,
                        'white'     : 35000,
                        'blue'      : 47000,
                        'pink'      : 56100
                    }

        #self.lights = [Light(i,args.ip_light) for i in range(1,4)]

        self.IP_RECV = args.ip_recv
        self.IP_SEND = args.ip_send
        self.PORT_RECV = int(args.port_recv)
        self.PORT_SEND = int(args.port_send)


    def execute_command(self, command):
        """[summary]
        
        [description]
        
        Arguments:
            command {[type]} -- [description]
        """

        #c = command.split('|')
        c = command[:5]

        #Obtains list of lights that with issued commands
        if c[1] == 'all':
            lamp = list(range(0,3))
        else:
            lamp = [int(i)-1 for i in c[1].split(',')]

        #Issue commands for each light
        for i in lamp:
            #State
            if c[2]:
                self.lights[i].set_status(True if c[2] == 'on' else False)

            #Color
            if c[3]:
                self.lights[i].set_color(self.color[c[3]])

            #Brightness
            if c[4]:
                bri = int((int(c[4])/100.0)*254) + \
                    (self.lights[i].get_bri() if c[4][0] in ('+','-') else 0)
                if bri > 254:
                    bri = 254
                elif bri < 0:
                    bri = 0
                self.lights[i].set_bri(bri)

            self.log.info(str(self.lights[i]))
            self.lights[i].update_light()


    def main(self):
        
        conn = connection.Client(self.IP_RECV, self.PORT_RECV)
        time_conn = connection.Client(self.IP_SEND,self.PORT_SEND)


        try:
            self.log.info('Opening connection')

            conn.connect()
            self.log.info('connected to VM!')

            time_conn.connect()
            self.log.info('connected to sensor!')

            count = 0
            while True:
                command = conn.receive_message()

                if command != None: 
                    self.log.info('Received: ' + str(command))
                else:
                    break

        except KeyboardInterrupt:
            self.log.debug('\nINTERRUPTED BY USER') 
        except Exception as e:
            self.log.debug("ERROR: " + str(e))
        finally:
            conn.destroy()
            time_conn.destroy()
            self.log.debug('Finishing program')



if(__name__ == '__main__'):
    try:
        parser = argparse.ArgumentParser(description='Actuator Node Logging')


        parser.add_argument('--ip-light', action="store", type=str, 
            default='192.168.0.10', 
            help='Select the ip through which the program will send data.')
        
        parser.add_argument('--ip-send', action="store", type=str, 
            default='192.168.0.38', 
            help='Select the ip through which the program will send data.')

        parser.add_argument('--ip-recv', action="store", type=str, 
            default='192.168.0.1', 
            help='Select the ip through which the program will receive data.')

        parser.add_argument('--port-send', action="store", default=5008, 
            help='Select the port through which the program will send data.')

        parser.add_argument('--port-recv', action="store", default=5008, 
            help='Select the port through which the program will receive audio data.')

        parser.add_argument('--log-level', action="store", type=str,
            choices=["error", "warning", "info", "debug", "notset"],
            default="info", help='Select the log level of the program.')

        parser.add_argument('--verbose', default=False, action = 'store_true',
            help='Select whether to output logs to the console.')

        args = parser.parse_args()

    except Exception as e:
        print 'ERROR PARSING ARGUMENTS'
    act = Actuator(args)
    act.main()
