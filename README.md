Smart Lights Over Voice Control
============================
###Dependencies
####Sensor
> sudo apt-get install python-pip  
> sudo pip install --upgrade pip  
> sudo pip install wheel  
> sudo apt-get install python-dev  
> sudo apt-get install python-pyaudio  

####Actuator
> sudo apt-get install python-pip  
> sudo pip install --upgrade pip  
> sudo pip install wheel  
> sudo pip install beautifulhue

###Usage
1. Clone the repository
> git clone https://futebolUFMG@bitbucket.org/futebolUFMG/voice_control.git

2. Copy the files to each agent
> cd voice_control  
> scp -r offline/sensor pi@**_IP_RASPBERRY_SENSOR_**:~  
> scp -r offline/VM **_USER_**@**_IP_VM_**:~  
> scp -r offline/Actuator pi@**_IP_RASPBERRY_ACTUATOR_**:~
 
3. On each agent, execute the main files  

* Raspberry (Sensor):  
**_VMs_AUDIO_PORT_** defaults to 5007, as in the port on which the VM will be expecting to receive audio data from the sensor. **_CMD_FEEDBACK_PORT_** defaults to 5008, as in the port on which the sensor is expecting to receive the commands feedback from the actuator.
> cd offline/Sensor  
> python sensor.py [--ip-send **_VMs_IP_**] [--port-send **_VMs_AUDIO_PORT_**] [--port-recv **__CMD_FEEDBACK_PORT_**] [--log-level **_LOG_LEVEL_**] [--verbose]

* VM (speech recognition)  
**_PORT_RECV_** and **_PORT_SEND_** default to 5007 and 5008 respectively.  
> cd offline/VM  
> python converter.py [--port-recv **_PORT_RECV _**] [--port-send **_PORT_SEND_**] [--log-level **_LOG_LEVEL_**] [--verbose]  

* Raspberry (Actuator):  
> cd offline/Actuator  
> python actuator.py [--ip-light **_LIGHTS_CONTROLLER_IP _**] [--ip-send **_SENSOR_NODE_IP_**] [--ip-recv **_VMs_IP_**] [--port-send **_SENSOR_PORT_**] [--port-recv **_VM_PORT_**] [--log-level **_LOG_LEVEL_**] [--verbose]  

* The values for **_LOG_LEVEL_** are [error,warning,info,debug,notset]  

#####**Commands:**  
* This system recognizes the following type of commands:  
> _LIGHTS_ (1|2|3)? (ON|OFF) ((_COLOR_)? **_COLOR_NAME_**)? (BRIGHTNESS **_BROPTIONS_**)?  

* The options for **_COLOR_NAME_** are: [red, blue, yellow, green, pink, white]  
* The options for **_BROPTIONS_** are: [up, down, low, medium, high]  
 * _Up_ and _Down_ respectively increases and decreases the current brightness value in 20%.  
 * _Low_ changes the brightness value to 0%. (That does not mean the lights are turned off, it means the brightness is dimmed to the minimum value it can achieve)  
 * _Medium_ changes the brightness value to 50%.  
 * _High_ changes the brightness value to 100%.